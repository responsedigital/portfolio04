<!-- Start Portfolio 04 -->
@if(!Fir\Utils\Helpers::isProduction())
<!-- Description : Scroll Spy Nav based on component IDs and Names -->
@endif
<div class="portfolio-04"  is="fir-portfolio-04">
  <script type="application/json">
      {
          "options": @json($options)
      }
  </script>
  <div class="portfolio-04__wrap">
      <ul class="portfolio-04__list">
          @for ($i = 0; $i < 6; $i++)
            <li class="portfolio-04__item">
                <a href="" class="portfolio-04__link">
                    Link Here
                </a>
            </li>
          @endfor
      </ul>
  </div>
</div>
<!-- End Portfolio 04 -->