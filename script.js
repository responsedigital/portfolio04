class Portfolio04 extends window.HTMLDivElement {

    constructor (...args) {
        const self = super(...args)
        self.init()
        return self
    }

    init () {
        this.props = this.getInitialProps()
        this.resolveElements()
      }
    
    getInitialProps () {
        let data = {}
        try {
            data = JSON.parse($('script[type="application/json"]', this).text())
        } catch (e) {}
        return data
    }

    resolveElements () {

    }

    connectedCallback () {
        this.initPortfolio04()
    }

    initPortfolio04 () {
        const { options } = this.props
        const config = {

        }

        console.log("Init: Portfolio 04")
    }

}

window.customElements.define('fir-portfolio-04', Portfolio04, { extends: 'div' })
